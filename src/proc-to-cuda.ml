open Exp
open Proto
open Common
open Serialize
open Cmdliner

(* ----------------- constants -------------------- *)
let thread_globals : VarSet.t =
  VarSet.of_list (List.map var_make
  ["gridDim.x"; "blockIdx.x"; "blockDim.x";
  "gridDim.y"; "blockIdx.y"; "blockDim.y";
  "gridDim.z"; "blockIdx.z"; "blockDim.z"])

(* ----------------- kernel functions -------------------- *)

let open_ic_with (fname:string option) (f : in_channel -> unit) : unit =
    let ic, (closer: in_channel -> unit) = match fname with
    | Some fname -> (open_in fname, close_in_noerr)
    | None -> (stdin, fun x -> ())
    in
    try (f ic; closer ic) with
    | e -> closer ic;
      raise e

let p_kernel_parser fname input : prog kernel =
  let fname = match fname with
  | Some x -> x
  | None -> "<STDIN>"
  in
  let filebuf = Lexing.from_channel input in
  Scan.set_filename filebuf fname;
  try Parse2.main Scan.read filebuf with
  | Parse2.Error ->
    let b = Buffer.create 1024 in
    let sloc = Sourceloc.of_lexbuf filebuf in
    Printf.bprintf b "%a: syntax error" Sourceloc.location_bprint_start sloc;
    (try
        Printf.bprintf b "%a" Sourceloc.location_bprint_title sloc
    with
        Sys_error _ -> ()
    );
    raise (Common.ParseError b)

let j_kernel_parser (_:string option) ic =
  try Yojson.Basic.from_channel ic |> Parsejs.parse_kernels.run with
  | Yojson.Json_error("Blank input data") ->
    (* If the input is blank, just ignore the input and err with -1 *)
    raise (Common.mk_parse_error_s "Empty input data. Blank file?\n")
  | Yojson.Json_error(e) ->
    raise (Common.mk_parse_error_s (Printf.sprintf "Error parsing JSON: %s\n" e))

type i_kernel =
  | JKernel of Imp.p_kernel list
  | PKernel of prog kernel

let parse_i_kernel (use_json:bool) (fname:string option) (ic:in_channel) : i_kernel =
  if use_json then
    JKernel (j_kernel_parser fname ic)
  else
    PKernel (p_kernel_parser fname ic)

let open_i_kernel_with (use_json:bool) (fname:string option) (f:i_kernel -> unit) : unit =
  open_ic_with fname (fun ic ->
    f (parse_i_kernel use_json fname ic)
  )

let i_kernel_to_p_kernel (k:i_kernel) : prog kernel list =
  match k with
  | JKernel ks -> List.map Imp.compile ks
  | PKernel p -> [p]

(* ----------------- serialization -------------------- *)

(* Gives the dummy variable string for any variable *)
let var_to_dummy (v:variable) : string =
  "__dummy" ^ var_name v

(* Maps a list of values to an index-separated string containing them *)
let idx_to_s (f:'a -> string) (l:'a list) : string =
  "[" ^ (Common.join "][" (List.map f l)) ^ "]"
  
(* Gives the dummy instruction string for any array read/write *)
let acc_expr_to_dummy (x, a) : PPrint.t list =
  let var = var_name x ^ idx_to_s PPrint.n_to_s a.access_index in
  [Line (match a.access_mode with
  | R -> var_to_dummy x ^ " = " ^ var ^ ";"
  | W -> var ^ " = " ^ var_to_dummy x ^ "_w();")]

(* Converts source instruction to a valid CUDA operation *)
let rec inst_to_s : inst -> PPrint.t list =
  let open PPrint in
  function
  | Sync -> [Line "__syncthreads();"]
  | Acc e -> acc_expr_to_dummy e
  | Cond (b, p1) -> [
      Line ("if (" ^ b_to_s b ^ ") {");
      Block (List.map inst_to_s p1 |> List.flatten);
      Line "}"
    ]
  | Loop (r, p) ->
    [
      Line ("for (" ^ "int " ^ ident r.range_var ^ " = " ^
      n_to_s r.range_lower_bound ^ "; " ^ ident r.range_var ^ " < " ^
      n_to_s r.range_upper_bound ^ "; " ^ ident r.range_var ^ " = " ^
      ident r.range_var ^ " " ^ s_to_s r.range_step ^ ") {");
      Block (List.map inst_to_s p |> List.flatten);
      Line "}"
    ]

let prog_to_s (p: prog) : PPrint.t list =
  List.map inst_to_s p |> List.flatten

(* Get the type of an array, defaulting to int if it is unknown *)
let arr_type (arr:array_t) : string =
  match arr.array_type with
  | [] -> "int"
  | _ -> Common.join " " arr.array_type

(* Helper functions for making the kernel header/parameters *)
let global_arr_to_l (vs:array_t VarMap.t) : string list =
  VarMap.bindings vs
  |> List.map (fun (k,v) -> arr_type v ^ " *" ^ var_name k)

let global_var_to_l (vs:VarSet.t) : string list =
  VarSet.elements (VarSet.diff vs thread_globals)
  |> List.map (fun v -> "int " ^ var_name v)

(* Helper functions for making kernel variable declarations/prototypes *)
let arr_to_proto (vs:array_t VarMap.t) : string list =
  VarMap.bindings vs
  |> List.map (fun (k,v) -> "    extern " ^
  arr_type v ^ " " ^ var_to_dummy k ^ "_w();")

let arr_to_shared (vs:array_t VarMap.t) : string list = 
  VarMap.bindings vs
  |> List.map (fun (k,v) -> "    " ^ (match v.array_size with
  | [] -> "extern " | _ -> "") ^ "__shared__ " ^ arr_type v ^ " " ^
  var_name k ^ idx_to_s string_of_int v.array_size ^ ";")

let arr_to_dummy (vs:array_t VarMap.t) : string list =
  VarMap.bindings vs
  |> List.map (fun (k,v) -> "    " ^ arr_type v ^ " " ^ var_to_dummy k ^ ";")

(* Serialization of the kernel *)
let kernel_to_s (f:'a -> PPrint.t list) (k:'a kernel) : PPrint.t list =
  let open PPrint in
  let arrays = (VarMap.partition (fun k -> fun v ->
    v.array_hierarchy = GlobalMemory) k.kernel_arrays) in
  let global_arr = global_arr_to_l (fst arrays) in
  let global_var = global_var_to_l k.kernel_global_variables in
  let dummy_var = arr_to_dummy k.kernel_arrays in
  let shared_arr = arr_to_shared (snd arrays) in
  let funct_protos = arr_to_proto k.kernel_arrays in
  [
    Line "__global__";
    Line ("void " ^ k.kernel_name ^ "(" ^ Common.join
    ", " (global_arr @ global_var) ^ ")");
    Line "{";
    Line (Common.join "\n" (dummy_var @ shared_arr @ funct_protos));
    Block (f k.kernel_code);
    Line "}"
  ]
  
let print_kernel (f:'a -> PPrint.t list) (k: 'a kernel) : unit =
  PPrint.print_doc (kernel_to_s f k)

let print_k (k:prog kernel) : unit =
  PPrint.print_doc (kernel_to_s prog_to_s k)

(* ----------------- command line argument parser -------------------- *)

let main_t =
  let use_json =
    let doc = "Parse a JSON file" in
    Arg.(value & flag & info ["json"] ~doc)
  in

  let get_fname =
    let doc = "The path $(docv) of the GPU contract." in
    Arg.(value & pos 0 (some string) None & info [] ~docv:"CONTRACT" ~doc)
  in

  let do_main
    (fname: string option)
    (use_json: bool)
    : unit =
  let print_cuda (k : i_kernel) : unit =
    List.iter (fun k -> print_k k) (i_kernel_to_p_kernel k)
  in
  try open_i_kernel_with use_json fname print_cuda with
  | Common.ParseError b ->
      Buffer.output_buffer stderr b;
      exit (-1) in
  Term.(
    const do_main
    $ get_fname
    $ use_json
  )

let info =
  let doc = "Generates a CUDA file from a protocol" in
  Term.info "proc-to-cuda" ~doc ~exits:Term.default_exits

(* ----------------- execution entry point -------------------- *)

let _ =
  Term.eval (main_t, info)
  |> Term.exit
